let collection = {};
let frontIdx = 0;
let rearIdx = 0;

function print() {
  const result = [];
  for (let i = frontIdx; i < rearIdx; i++) {
    result[result.length] = collection[i];
  }
  return result;
}

function enqueue(item) {
  collection[rearIdx] = item;
  rearIdx++;
  return print();
}

function dequeue() {
  if (frontIdx === rearIdx) {
    // Queue is empty
    return print();
  }

  const frontItem = collection[frontIdx];
  frontIdx++;

  const newCollection = {};
  let newIndex = 0;
  for (let i = frontIdx; i < rearIdx; i++) {
    newCollection[newIndex] = collection[i];
    newIndex++;
  }
  collection = newCollection;

  rearIdx = newIndex;
  frontIdx = 0;

  return print();
}

function front() {
    return collection[0];
}

function size() {
  return rearIdx - frontIdx;
}

function isEmpty() {
    return collection.length === 0;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};
